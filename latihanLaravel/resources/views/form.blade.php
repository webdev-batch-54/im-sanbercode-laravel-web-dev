<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>reg</title>
</head>
<body>
    <form action="/welcome" method="POST">
        @csrf
        <label>First name:</label> <br> <br>
        <input type="text" name="fname"> <br> <br>
        <label>Last name:</label> <br> <br>
        <input type="text" name="lname"> <br> <br>
        <label>Gender:</label> <br> <br>
        <input type="radio" name="Gender">Male <br>
        <input type="radio" name="Gender">Female <br>
        <input type="radio" name="Gender">Other <br> <br>
        <label>Nationality:</label> <br> <br>
        <select name="nationality">
            <option value="">Indonesia</option>
            <option value="">Singapura</option>
            <option value="">Saudi Arabia</option>
            <option value="">Malaysia</option>
        </select> <br> <br>
        <label>Language Spoken:</label> <br> <br>
        <input type="checkbox" name="spoken"> Bahasa Indonesia <br>
        <input type="checkbox" name="spoken"> Bahasa Inggris <br>
        <input type="checkbox" name="spoken"> Bahasa Lainnya <br> <br>
        <label>Bio</label> <br> <br>
        <textarea cols="30" rows="10"></textarea> <br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>