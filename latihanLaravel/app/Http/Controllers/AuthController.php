<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function signup() {
        return view('form');
    }
    public function welcome(Request $request) {
        $namaDepan = $request['fname'];
        $namaBelakang = $request['lname'];

        return view('selamat', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
