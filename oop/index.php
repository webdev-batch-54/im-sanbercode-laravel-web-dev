<?php
// require_once 'animal.php';
// $sheep = new Animal("shaun");
// echo "<h3>Release 0 OOP</h3><br>";
// echo "Nama: " . $sheep->get_name();

require_once 'Animal.php';
require_once 'Frog.php';
require_once 'Ape.php';

$sheep = new Animal("shaun");
echo "Nama Hewan : " . $sheep->get_name() . "<br>";
echo "legs : " . $sheep->get_legs() . "<br>";
echo "cold blooded : " . $sheep->get_cold_blooded() . "<br><br>";

$kodok = new Frog("buduk");
echo "Nama Hewan : " . $kodok->get_name() . "<br>";
echo "legs : " . $kodok->get_legs() . "<br>";
echo "cold blooded : " . $kodok->get_cold_blooded() . "<br>";
echo "Jump : ";
$kodok->jump(); // "Hop Hop"
echo "<br><br>";

$sungokong = new Ape("kera sakti");
echo "Nama Hewan : " . $sungokong->get_name() . "<br>";
echo "legs : " . $sungokong->get_legs() . "<br>";
echo "cold blooded : " . $sungokong->get_cold_blooded() . "<br>";
echo "Yell : ";
$sungokong->yell(); // "Auooo"
echo "<br>";

?>
