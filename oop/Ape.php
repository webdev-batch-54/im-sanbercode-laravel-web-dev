<?php

require_once 'Animal.php';

class Ape extends Animal
{
    protected $legs = 2;

    public function yell()
    {
        echo "Auooo";
    }
}

?>
